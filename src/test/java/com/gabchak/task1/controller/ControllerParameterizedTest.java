package com.gabchak.task1.controller;

import com.gabchak.task1.model.Sequence;
import com.gabchak.task1.view.ConsoleReader;
import com.gabchak.task1.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class ControllerParameterizedTest {

    @Mock
    private View view;

    @Mock
    private ConsoleReader reader;

    @InjectMocks
    private Controller controller;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Parameterized.Parameters
    public static Collection inputAndExpectedResult()  {
        return Arrays.asList(new Object[][] {
                {"3 5 6 7 7 7 7 2 9", new Sequence(7, 3, 4)},
                {"3 3 3 2 4", new Sequence(0, 0, 0)},
                {"3 4 4 4 3 5 5 5 5 5 2", new Sequence(5, 5, 5)},
                {"3 4 4 4 3 5 5 5 5 5 6", new Sequence(4, 1, 3)},
                {"4 4 4 3 5 5 5 5 5 6", new Sequence(0, 0, 0)},
                {"4 4 4 3 5 5 5 5 5 ", new Sequence(0, 0, 0)},
                {"4 4 4 3 234 234 234 5 5 5 ", new Sequence(234, 4, 3)},
                {"4 5 4 3 2", new Sequence(5, 1, 1)}
        });
    }

    private Sequence expected;
    private String inputString;

    public ControllerParameterizedTest(String inputString, Sequence expected) {
        this.expected = expected;
        this.inputString = inputString;
    }

    @Test
    public void testStart() {
        when(reader.readLine()).thenReturn(inputString);

        controller.start();

        verify(reader).readLine();
        verify(view).print(expected);
    }
}

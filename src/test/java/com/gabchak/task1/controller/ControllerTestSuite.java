package com.gabchak.task1.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
    ControllerLongestSequenceTest.class,
    ControllerParameterizedTest.class
})
public class ControllerTestSuite {
}

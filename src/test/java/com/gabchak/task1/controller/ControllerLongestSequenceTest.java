package com.gabchak.task1.controller;

import com.gabchak.task1.model.Sequence;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class ControllerLongestSequenceTest {

    public Object[] numbersWithoutSequences() {
        return new Object[][]{
                {List.of(4, 5, 6, 7, 8, 9)},
                {List.of(4, 4, 2, 5, 6, 7, 7, 7)},
                {List.of(2, 3, 4, 5, 6, 7)},
                {List.of(7, 6, 5, 4, 3, 2)},
                {List.of(4, 5, 5, 5, 5, 7)},
                {List.of(2, 1, 2, 3)}
        };
    }

    @Test
    @Parameters(method = "numbersWithoutSequences")
    public void shouldNotFindSequence(List<Integer> numbers) {
        Sequence actualSequence =
                Controller.getLongestSequenceBetweenSmallerNumbers(numbers);

        Assert.assertEquals(0, actualSequence.getSize());
        Assert.assertTrue(actualSequence.getValue() == 0);
        Assert.assertFalse(actualSequence.getStartIndex() > 0);
    }

    @Test
    public void shouldSelectTheLongestSequence() {
        List<Integer> numbers =
                List.of(1, 2, 2, 1, 5, 5, 5, 5, 5, 4, 7, 7, 7, 3);
        Sequence actualSequence =
                Controller.getLongestSequenceBetweenSmallerNumbers(numbers);

        Assert.assertEquals("Number", 5, actualSequence.getValue());
        Assert.assertEquals("Start index", 4, actualSequence.getStartIndex());
        Assert.assertEquals("Size", 5, actualSequence.getSize());
    }
}

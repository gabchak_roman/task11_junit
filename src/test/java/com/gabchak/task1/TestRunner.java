package com.gabchak.task1;

import com.gabchak.task1.controller.ControllerTestSuite;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class TestRunner {
    public static void main(String[] args) {

        Result result = JUnitCore.runClasses(ControllerTestSuite.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println("Was successful: " + result.wasSuccessful());
    }
}

package com.gabchak.task2;

import com.gabchak.task2.controller.MinesweeperTestSuite;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class TestRunner {
    public static void main(String[] args) {

        Result result = JUnitCore.runClasses(MinesweeperTestSuite.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println("Was successful: " + result.wasSuccessful());
    }
}

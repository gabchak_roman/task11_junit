package com.gabchak.task2.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
    MinesweeperTest.class,
})
public class MinesweeperTestSuite {
}

package com.gabchak.task2.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MinesweeperTest {
    public Object[] fieldConfigurations() {
        return new Object[][]{
                {5, 5, 0.5f},
                {3, 7, 0.2f},
                {10, 20, 0.8f},
                {5, 2, 0.1f}
        };
    }

    @Test
    @Parameters(method = "fieldConfigurations")
    public void testBuildMineField(int M, int N, float p) {
        boolean filed[][] =
                Minesweeper.buildMineField(M, N, p);

        Assert.assertEquals(M+2, filed.length);
        Assert.assertEquals(N+2, filed[0].length);

        int bombCount = 0;
        for (int i = 1; i < M+1; i++) {
            for (int j = 1; j < N+1; j++) {
                if (filed[i][j]) {
                    ++bombCount;
                }
            }
        }
        float actualProbability = (float) bombCount / (M*N);
        Assert.assertEquals("Probability", p, actualProbability, 0.1);
    }

    @Test
    public void testNeighboringBombsCount() {
        boolean[][] field = new boolean[][] {
                {false, false, false, false, false, false},
                {false,  true,  true,  true, false, false},
                {false, false,  true, false, false, false},
                {false,  true,  true, false,  true, false},
                {false, false, false, false, false, false},
                {false, false, false, false,  true, false},
                {false, false, false, false, false, false},
        };

        int expected[][] = new int[][] {
                {0,  0,  0,  0,  0,  0},
                {0, -1, -1, -1,  1,  0},
                {0,  5, -1,  5,  2,  0},
                {0, -1, -1,  3, -1,  0},
                {0,  2,  2,  3,  2,  0},
                {0,  0,  0,  1, -1,  0},
                {0,  0,  0,  0,  0,  0}
        };
        int M = 5;
        int N = 4;

        int actual[][] = Minesweeper.getNeighboringBombsCount(field, M, N);

        Assert.assertEquals("M", M+2, actual.length);
        Assert.assertEquals("N", N+2, actual[0].length);

        for (int i = 1; i < M + 1; i++) {
            for (int j = 1; j < N + 1; j++) {
                Assert.assertEquals("Neighbor count", expected[i][j],
                        actual[i][j]);
            }
        }
    }
}

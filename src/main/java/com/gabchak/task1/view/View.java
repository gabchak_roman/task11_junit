package com.gabchak.task1.view;

import com.gabchak.task1.model.Sequence;

public interface View {

    void printMessage(String message);

    void print(Sequence sequence);
}

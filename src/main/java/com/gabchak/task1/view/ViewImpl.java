package com.gabchak.task1.view;

import com.gabchak.task1.model.Sequence;

public class ViewImpl implements View {

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public final void printMessage(final String message) {
        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }

    @Override
    public void print(Sequence sequence) {
        if (sequence.isFound()) {
            printMessage("Longest sequence of numbers: " + sequence.getValue());
            System.out.println("Size: " + sequence.getSize());
            System.out.println("Start index: " + sequence.getStartIndex());
        } else {
            System.out.println("Sequence not found.");
        }
    }
}


package com.gabchak.task1.controller;

import com.gabchak.task1.model.Sequence;
import com.gabchak.task1.view.ConsoleReader;
import com.gabchak.task1.view.View;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Controller {
    private View view;

    private ConsoleReader reader;

    public Controller(View viewIn, ConsoleReader reader) {
        this.view = viewIn;
        this.reader = reader;
    }

    public static Sequence getLongestSequenceBetweenSmallerNumbers(
            List<Integer> numbers) {
        Sequence longestSequence = new Sequence();
        int startIndex = 0;
        int sequenceSize = 0;
        int sequenceNumber = 0;
        int prevNumber = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            int currentNumber = numbers.get(i);
            if (prevNumber < currentNumber) {
                sequenceNumber = currentNumber;
                sequenceSize = 1;
                startIndex = i;
            } else if (startIndex > 0) {
                if (prevNumber == currentNumber) {
                    ++sequenceSize;
                } else if (prevNumber > currentNumber) {
                    if (longestSequence.getSize() < sequenceSize) {
                        longestSequence.setSize(sequenceSize);
                        longestSequence.setValue(sequenceNumber);
                        longestSequence.setStartIndex(startIndex);
                    }
                }
            }
            prevNumber = currentNumber;
        }
        return longestSequence;
    }

    public void start() {
        view.printMessage("Enter comma/space separated numbers: ");
        String line = reader.readLine();
        List<Integer> numbers = Arrays.stream(line.split("\\s*,\\s*|\\s+"))
                .filter(Objects::nonNull)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Sequence sequence = getLongestSequenceBetweenSmallerNumbers(numbers);

        view.print(sequence);
    }
}


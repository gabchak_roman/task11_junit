package com.gabchak.task1.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.StringJoiner;

public class Sequence {
    private int size;
    private int startIndex;
    private int value;

    public Sequence() {

    }

    public Sequence(int value, int startIndex, int size) {
        this.value = value;
        this.size = size;
        this.startIndex = startIndex;
    }


    public Boolean isFound() {
        return size > 0;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Sequence sequence = (Sequence) o;

        return new EqualsBuilder().append(size, sequence.size)
                .append(startIndex, sequence.startIndex).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(size).append(startIndex)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Sequence.class.getSimpleName() + "[", "]")
                .add("size=" + size).add("startIndex=" + startIndex).toString();
    }


}

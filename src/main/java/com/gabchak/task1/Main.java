package com.gabchak.task1;

import com.gabchak.task1.controller.Controller;
import com.gabchak.task1.view.ConsoleReader;
import com.gabchak.task1.view.ViewImpl;

public class Main {
    public static void main(String[] args) {
        Controller controller =
                new Controller(new ViewImpl(), new ConsoleReader());
        controller.start();
    }
}

package com.gabchak.task2;


import com.gabchak.task2.controller.Minesweeper;
import com.gabchak.task2.view.ConsoleReader;
import com.gabchak.task2.view.ViewImpl;

public class Main {
    public static void main(String[] args) {
        Minesweeper controller =
                new Minesweeper(new ViewImpl(), new ConsoleReader());
        controller.start();
    }
}

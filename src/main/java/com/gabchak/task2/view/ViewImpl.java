package com.gabchak.task2.view;

public class ViewImpl implements View {

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public final void printMessage(final String message) {
        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }


    @Override
    public void print(boolean[][] field) {
        System.out.println("Bomb field.");
        for (int i = 1; i < field.length - 1; i++) {
            for (int j = 1; j < field[0].length - 1; j++) {
                if (field[i][j]) {
                    System.out.printf("%3s", "*");
                } else {
                    System.out.printf("%3s", ".");
                }
            }
            System.out.println();
        }
    }

    @Override
    public void printNeighboringBombsCount(int[][] neighboringBombsCount) {
        System.out.println("Neighboring bomb count:");
        for (int i = 1; i < neighboringBombsCount.length - 1; i++) {
            for (int j = 1; j < neighboringBombsCount[0].length - 1; j++) {
                int count = neighboringBombsCount[i][j];
                if (count < 0) {
                    System.out.printf("%3s", "*");
                } else {
                    System.out.printf("%3d", count);
                }
            }
            System.out.println();
        }
    }
}


package com.gabchak.task2.view;

import com.gabchak.task2.model.Sequence;

public interface View {

    void printMessage(String message);

    void print(boolean[][] sequence);

    void printNeighboringBombsCount(int[][] neighboringBombsCount);
}

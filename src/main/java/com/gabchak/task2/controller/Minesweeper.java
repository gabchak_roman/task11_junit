package com.gabchak.task2.controller;

import com.gabchak.task2.view.ConsoleReader;
import com.gabchak.task2.view.View;


public class Minesweeper {
    private View view;

    private ConsoleReader reader;

    public Minesweeper(View viewIn, ConsoleReader reader) {
        this.view = viewIn;
        this.reader = reader;
    }

    public void start() {
        view.printMessage("Enter M, N and probability (comma/space separated): ");
        String line = reader.readLine();
        String[] args = line.split("\\s*,\\s*|\\s+");
        if (args.length != 3) {
            view.printMessage("Should be 3 args.");
        }
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        float probability = Float.parseFloat(args[2]);

        boolean[][] field = buildMineField(M, N, probability);

        view.print(field);

        int[][] neighboringBombsCount = getNeighboringBombsCount(field, M, N);

        view.printNeighboringBombsCount(neighboringBombsCount);
    }

    public static boolean[][] buildMineField(int M, int N, float probability) {
        boolean field[][] = new boolean[M+2][N+2];

        for (int i = 1; i < M+1; i++) {
            for (int j = 1; j < N+1; j++) {
                field[i][j] = Math.random() <= probability;
            }
        }
        return field;
    }

    public static int[][] getNeighboringBombsCount(boolean[][] field, int M, int N){
        int bombCountField[][] = new int[M+2][N+2];

        for (int i = 1; i < M+1; i++) {
            for (int j = 1; j < N+1; j++) {
                int count = -1;
                if (!field[i][j]) {
                    count = toInt(field[i-1][j-1]) + toInt(field[i-1][j]) + toInt(field[i-1][j+1]) +
                            toInt(field[i][j-1]) + toInt(field[i][j+1]) +
                            toInt(field[i+1][j-1]) + toInt(field[i+1][j]) + toInt(field[i+1][j+1]);
                }
                bombCountField[i][j] = count;
            }
        }
        return bombCountField;
    }

    private static int toInt(boolean b) {
        return b ? 1 : 0;
    }
}

